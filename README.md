
# Reign Test 

Web application dockerized

Server: NodeJS + NestJS + MongoDB

Client: ReactJS


## API Reference
#### Import data

```http
  GET /post/import-data
```
The information will be updated from the url: https://hn.algolia.com/api/v1/search_by_date?query=nodejs

#### Get all posts

```http
  GET /post/all
```

#### Delete post

```http
  DELETE /post/delete/:postId
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `postId`      | `string` | _id filed mongoDB |


## Deployment

To deploy this project run

```bash
  docker-compose up -d --build
```


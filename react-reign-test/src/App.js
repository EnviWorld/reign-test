import logo from "./logo.svg";
import Header from "./components/header/Header";
import ListPost from "./components/list-post/ListPost";

function App() {
    return (
        <div>
            <Header />
            <ListPost />
        </div>
    );
}

export default App;

import React, { useState, useEffect } from "react";
import Moment from "react-moment";
import { AiFillDelete } from "react-icons/ai";
import "./ListPost.css";

function ListPost() {
    const [err, setErr] = useState("");
    const [data, setData] = useState([]);

    const deletePost = async (postID) => {
        const res = await fetch(`http://localhost:5000/post/delete/${postID}`, { method: "DELETE" });
        const json = await res.json();
        setData(json["posts"]);
    };

    const fetchData = async () => {
        const res = await fetch("http://localhost:5000/post/all");
        const json = await res.json();
        if (json["posts"].length > 0) {
            setData(json["posts"]);
        } else {
            refreshData();
        }
    };

    const refreshData = async () => {
        const res = await fetch("http://localhost:5000/post/import-data");
        const json = await res.json();
        console.log(json);
        setData(json["posts"]);
    };

    useEffect(() => {
        fetchData();
        setInterval(() => {
            refreshData();
        }, 3600000);
    }, [setData]);

    return (
        <ul>
            {data.map((item, index) => {
                if (item.active !== false) {
                    return (
                        <li key={item._id}>
                            <div className="container-title">
                                <a href={item.story_url ? item.story_url : item.url} className="url" target="_blank">
                                    <span className="story_title">{`${
                                        item.story_title ? item.story_title : item.title
                                    } - `}</span>
                                    <span className="story_author">{item.author}</span>
                                </a>
                            </div>
                            <div className="container-action">
                                <Moment date={item.created_at} fromNow className="story_title"></Moment>
                            </div>
                            <div className="container-action">
                                <a onClick={() => deletePost(item._id)} className="btn-delete">
                                    <AiFillDelete />
                                </a>
                            </div>
                        </li>
                    );
                }
            })}
        </ul>
    );
}

export default ListPost;

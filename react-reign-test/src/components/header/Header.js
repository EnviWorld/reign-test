import React, { useState } from "react";
import "./Header.css";

function Header() {
    const [title, setTitle] = useState("HN Feed");
    const [subTitle, setSubTitle] = useState("We <3 hacker news!");
    return (
        <div className="main">
            <h1>{title}</h1>
            <h3>{subTitle}</h3>
        </div>
    );
}

export default Header;

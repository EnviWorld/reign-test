import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
} from '@nestjs/common';
import { PostService } from './post.service';

@Controller('post')
export class PostController {
  constructor(private postService: PostService) {}

  @Get('/all')
  async getAllPost(@Res() res) {
    const posts = await this.postService.getAllPost();
    return res.status(HttpStatus.OK).json({ posts });
  }

  @Delete('/delete/:postId')
  async deletePost(@Res() res, @Param('postId') postId) {
    let message = '';
    const deletedPost = await this.postService.updatePost(postId);
    console.log(deletedPost);
    if (deletedPost.updatedPost.modifiedCount > 0) {
      message = 'Se ha eliminado con exito';
    }
    const posts = await this.postService.getAllPost();
    return res.status(HttpStatus.OK).json({ message, posts });
  }

  @Get('/import-data')
  async importDataPost(@Res() res) {
    let objectsInserted = 0;
    const data = await this.postService.findAll();
    const response = await this.postService.saveImportData(data);
    if (response.status === 'ok') {
      objectsInserted = response.response.length;
    } else if (response.status === 'nok') {
      objectsInserted = response.response.result.nInserted;
    }
    const posts = await this.postService.getAllPost();
    return res
      .status(HttpStatus.OK)
      .json({ message: 'Ok', objectsInserted, posts });
  }
}

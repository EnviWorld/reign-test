import { Schema } from 'mongoose';

export const PostSchema = new Schema({
  title: String,
  url: String,
  author: String,
  points: Number,
  story_text: String,
  comment_text: String,
  num_comments: Number,
  story_id: { type: Number, unique: true },
  story_title: String,
  story_url: String,
  parent_id: Number,
  created_at_i: Number,
  _tags: [String],
  objectID: { type: String, unique: true },
  _highlightResult: Schema.Types.Mixed,
  created_at: String,
  active: Boolean,
});

import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

import { Post } from './interfaces/post.interface';
import { PostDTO } from './dto/post.dto';

import { AxiosResponse } from 'axios';
import { HttpService } from '@nestjs/axios';
import { Observable, map, switchMap } from 'rxjs';

@Injectable()
export class PostService {
  constructor(
    @InjectModel('Post') private postModel: Model<Post>,
    private httpService: HttpService,
  ) {}

  async getAllPost(): Promise<Post[]> {
    const posts = await this.postModel.find().sort({ created_at: 'desc' });
    return posts;
  }

  async getPost(postId: string): Promise<Post> {
    const post = await this.postModel.findById(postId);
    return post;
  }

  async updatePost(postId: string): Promise<any> {
    try {
      const updatedPost = await this.postModel.updateOne(
        { _id: postId },
        { active: false },
      );
      return { status: 'ok', updatedPost };
    } catch (e) {
      return { status: 'nok', response: e };
    }
  }

  async findAll(): Promise<any> {
    const response = await this.httpService
      .get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .toPromise();
    return response.data.hits;
  }

  async saveImportData(data: Post[]) {
    try {
      const response = await this.postModel.insertMany(data, {
        ordered: false,
      });
      return { status: 'ok', response };
    } catch (e) {
      return { status: 'nok', response: e };
    }
  }
}

import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PostModule } from './post/post.module';
import { MongooseModule } from '@nestjs/mongoose';

const url = process.env.MONGO_URL || 'localhost';

@Module({
  imports: [
    PostModule,
    MongooseModule.forRoot(
      `mongodb://reign-test:5*F.kthuKJ2N,%2F%218@${url}:27017/reign-test-db`,
    ),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
